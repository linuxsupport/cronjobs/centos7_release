# centos7_release

- Deprecated as of 30.06.2024

* Automated CC7 release process via nomad
* Uses the same scripts documented here: [linuxops](https://linuxops.web.cern.ch/distributions/cc7/)

